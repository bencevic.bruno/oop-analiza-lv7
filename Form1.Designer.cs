ï»¿namespace AnalizaLV7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_playerX = new System.Windows.Forms.Label();
            this.label_turn = new System.Windows.Forms.Label();
            this.label_playerO = new System.Windows.Forms.Label();
            this.picture_grid = new System.Windows.Forms.PictureBox();
            this.button_newGame = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Start = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picture_grid)).BeginInit();
            this.SuspendLayout();
            // 
            // label_playerX
            // 
            this.label_playerX.AutoSize = true;
            this.label_playerX.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_playerX.Location = new System.Drawing.Point(12, 9);
            this.label_playerX.Name = "label_playerX";
            this.label_playerX.Size = new System.Drawing.Size(126, 29);
            this.label_playerX.TabIndex = 0;
            this.label_playerX.Text = "Player X :";
            this.label_playerX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_turn
            // 
            this.label_turn.AutoSize = true;
            this.label_turn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_turn.Location = new System.Drawing.Point(318, 9);
            this.label_turn.Name = "label_turn";
            this.label_turn.Size = new System.Drawing.Size(99, 29);
            this.label_turn.TabIndex = 1;
            this.label_turn.Text = "TURN: ";
            this.label_turn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_playerO
            // 
            this.label_playerO.AutoSize = true;
            this.label_playerO.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_playerO.Location = new System.Drawing.Point(616, 9);
            this.label_playerO.Name = "label_playerO";
            this.label_playerO.Size = new System.Drawing.Size(128, 29);
            this.label_playerO.TabIndex = 2;
            this.label_playerO.Text = "Player O :";
            this.label_playerO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // picture_grid
            // 
            this.picture_grid.Location = new System.Drawing.Point(100, 50);
            this.picture_grid.MaximumSize = new System.Drawing.Size(600, 600);
            this.picture_grid.MinimumSize = new System.Drawing.Size(600, 600);
            this.picture_grid.Name = "picture_grid";
            this.picture_grid.Size = new System.Drawing.Size(600, 600);
            this.picture_grid.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picture_grid.TabIndex = 3;
            this.picture_grid.TabStop = false;
            this.picture_grid.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picture_grid_MouseUp);
            // 
            // button_newGame
            // 
            this.button_newGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_newGame.Location = new System.Drawing.Point(100, 656);
            this.button_newGame.Name = "button_newGame";
            this.button_newGame.Size = new System.Drawing.Size(600, 30);
            this.button_newGame.TabIndex = 4;
            this.button_newGame.Text = "New Game";
            this.button_newGame.UseVisualStyleBackColor = true;
            this.button_newGame.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_newGame_MouseUp);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(745, 166);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(851, 166);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(776, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Insert Player Names";
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(813, 194);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(75, 23);
            this.Start.TabIndex = 8;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 753);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button_newGame);
            this.Controls.Add(this.picture_grid);
            this.Controls.Add(this.label_playerO);
            this.Controls.Add(this.label_turn);
            this.Controls.Add(this.label_playerX);
            this.MaximumSize = new System.Drawing.Size(1000, 800);
            this.MinimumSize = new System.Drawing.Size(800, 800);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picture_grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_playerX;
        private System.Windows.Forms.Label label_turn;
        private System.Windows.Forms.Label label_playerO;
        private System.Windows.Forms.PictureBox picture_grid;
        private System.Windows.Forms.Button button_newGame;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Start;
    }
}

