ï»¿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnalizaLV7
{
    public partial class Form1 : Form
    {
        Graphics graphics;
        Pen pen = new Pen(Color.Black, 10);

        // 0 = O, 1 = +, 2 = empty;
        int[,] grid;

        // 0 = O, 1 = +;
        int playerTurn;
        int turnsPlayed;
        int winsX = 0, winsO = 0;
        int cellSize;
        string p1 = "O", p2 = "X"; //names;

        public Form1()
        {
            InitializeComponent();
            
            graphics = picture_grid.CreateGraphics();
            cellSize = (int) graphics.VisibleClipBounds.Width / 3;
            setupGame();
        }

        private void drawGrid()
        {
            graphics.DrawLine(pen, cellSize, 0, cellSize, 600);
            graphics.DrawLine(pen, 2 * cellSize, 0, 2 * cellSize, 600);
            graphics.DrawLine(pen, 0, cellSize, 600, cellSize);
            graphics.DrawLine(pen, 0, 2 * cellSize, 600, 2 * cellSize);
        }

        private void setupGame()
        {
            playerTurn = new Random().Next(0, 2);
            turnsPlayed = 0;
            grid = new int[3, 3] { { 2, 2, 2 }, { 2, 2, 2 }, { 2, 2, 2 } };
            graphics.Clear(Color.White);
            drawGrid();
            label_playerO.Text = "Player " + p1 + ": " + winsO;
            label_playerX.Text = "Player " + p2 + ": " + winsX;
            label_turn.Text = "Player " + (playerTurn == 0 ? p1 : p2) + "'s Turn";
        }

        private void picture_grid_MouseUp(object sender, MouseEventArgs e)
        {
            drawSymbol(new Point(e.X, e.Y));
            grid[e.Y / cellSize, e.X / cellSize] = playerTurn;
            if (checkVictory())
            {
                MessageBox.Show("Player " + (playerTurn == 0 ? p1 : p2) + " Won!", "Victory!");
                if (playerTurn == 0) winsO++;
                if (playerTurn == 1) winsX++;

                setupGame();
            }
            else
            {
                playerTurn = playerTurn == 1 ? 0 : 1;
                label_turn.Text = "Player " + (playerTurn == 0 ? p1 : p2) + "'s Turn";
                turnsPlayed++;


                if (turnsPlayed == 9)
                {
                    MessageBox.Show("Stalemate", "Game Over");
                    setupGame();
                }
            }
        }

        bool checkVictory()
        {
            bool combo0 = grid[0, 0] == playerTurn && grid[0, 1] == playerTurn && grid[0, 2] == playerTurn;
            bool combo1 = grid[1, 0] == playerTurn && grid[1, 1] == playerTurn && grid[1, 2] == playerTurn;
            bool combo2 = grid[2, 0] == playerTurn && grid[2, 1] == playerTurn && grid[2, 2] == playerTurn;

            bool combo3 = grid[0, 0] == playerTurn && grid[1, 0] == playerTurn && grid[2, 0] == playerTurn;
            bool combo4 = grid[0, 1] == playerTurn && grid[1, 1] == playerTurn && grid[2, 1] == playerTurn;
            bool combo5 = grid[0, 2] == playerTurn && grid[1, 2] == playerTurn && grid[2, 2] == playerTurn;

            bool combo6 = grid[0, 0] == playerTurn && grid[1, 1] == playerTurn && grid[2, 2] == playerTurn;
            bool combo7 = grid[0, 2] == playerTurn && grid[1, 1] == playerTurn && grid[2, 0] == playerTurn;

            return combo0 || combo1 || combo2 || combo3 || combo4 || combo5 || combo6 || combo7;
        }

        private void button_newGame_MouseUp(object sender, MouseEventArgs e)
        {
            setupGame();
        }

        private void Start_Click(object sender, EventArgs e)
        {
            p1 = textBox1.Text;
            p2 = textBox2.Text;
            setupGame();
        }

        private void drawSymbol(Point cellCenter)
        {
            if (playerTurn == 0)
            {
                graphics.DrawEllipse(pen, cellCenter.X - cellSize / 2, cellCenter.Y - cellSize / 2, cellSize * 0.75f, cellSize * 0.75f);
            }
            else
            {
                graphics.DrawLine(pen, cellCenter.X - cellSize * 0.3f, cellCenter.Y - cellSize * 0.3f, cellCenter.X + cellSize * 0.3f, cellCenter.Y + cellSize * 0.3f);
                graphics.DrawLine(pen, cellCenter.X - cellSize * 0.3f, cellCenter.Y + cellSize * 0.3f, cellCenter.X + cellSize * 0.3f, cellCenter.Y - cellSize * 0.3f);
            }
        }
    }
}
